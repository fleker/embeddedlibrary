var searchData=
[
  ['uart1_5frx_5fpin',['UART1_RX_PIN',['../group__hal__pic32mx250f128b.html#gab9130e621f608f1ee7c2c402571c88cd',1,'hal_pic32mx250f128b.h']]],
  ['uart1_5ftx_5fpin',['UART1_TX_PIN',['../group__hal__pic32mx250f128b.html#ga37b225cce113e8cf383598b1529e4e46',1,'hal_pic32mx250f128b.h']]],
  ['uart2_5frx_5fpin',['UART2_RX_PIN',['../group__hal__pic32mx250f128b.html#gae9b18b9e97c7d21973d66178f619b9de',1,'hal_pic32mx250f128b.h']]],
  ['uart2_5ftx_5fpin',['UART2_TX_PIN',['../group__hal__pic32mx250f128b.html#ga7f1359dcf7846039e33a5061f5e18f0e',1,'hal_pic32mx250f128b.h']]],
  ['uart_5finterrupt_5fpriority',['UART_INTERRUPT_PRIORITY',['../group__hal__pic32mx250f128b.html#gaeeb3ed40ef37d59b5deb15311a39f29e',1,'hal_pic32mx250f128b.h']]]
];
