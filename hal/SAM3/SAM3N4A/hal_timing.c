#include <stdint.h>
#include <stdbool.h>

#include "system.h"
#include "macros.h"



#ifndef TIMING_TIMER
#define TIMING_TIMER 1
#endif

#define TIMING_BASE CAT3(TIMER,TIMING_TIMER,_BASE)

void hal_timing_isr(void);

/* Initializes a timer peripheral with a period
of 1ms. For Cortex-M cores we can use the built-in
SysTick interrupt
*/
void hal_Timing_Init(void){

 SysTick_Config(FCPU/1000);
}

/* Initializes a timer peripheral with a period
of 1ms. For Cortex-M cores we can use the built-in
SysTick interrupt
*/
void hal_timing_isr(void) {
	
}

/* Not sure if anything needs to be done with the interrupt*/
void SysTick_Handler (void){
	hal_timing_isr();
}

#define TICKS_PER_US (FCPU/1000000)
#define TimingUsNow() (SysTick->VAL/TICKS_PER_US)

