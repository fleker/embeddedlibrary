#include <stdint.h>
#include <stdbool.h>
#include "hal_uart.h"
#include "system.h"
#include "chip.h"


void hal_UART0_ISR(void);
void hal_UART1_ISR(void);
void hal_UART2_ISR(void);
void hal_UART3_ISR(void);
void hal_UART4_ISR(void);
void hal_UART5_ISR(void);
void hal_UART6_ISR(void);
void hal_UART7_ISR(void);

Usart* g_hal_usart_base[2]={USART0, USART1 };
Uart* g_hal_uart_base[2]={UART0, UART1};

/*
UART Enable
*/
void hal_UART_Enable(uint8_t channel){
	if(channel<2){
		g_hal_uart_base[channel]->UART_CR = UART_CR_RXEN | UART_CR_TXEN;
	}else{
		g_hal_usart_base[channel%2]->US_CR = US_CR_RXEN | US_CR_TXEN;
	}
}

/*
UART Disable
*/
void hal_UART_Disable(uint8_t channel){
	if(channel<2){
		g_hal_uart_base[channel]->UART_CR = UART_CR_RSTRX | UART_CR_RSTTX| UART_CR_RXDIS | UART_CR_TXDIS;
	}else{
		g_hal_usart_base[channel%2]->US_CR = US_CR_RSTRX | US_CR_RSTTX| US_CR_RXDIS | US_CR_TXDIS;
	}
}
/*
UART Space Available
*/
uint8_t hal_UART_SpaceAvailable(uint8_t channel){
	
		if(channel<2){
			return((g_hal_uart_base[channel]->UART_SR & UART_SR_TXEMPTY) == 0);
		}else{
		return ((g_hal_usart_base[channel%2]->US_CSR & US_CSR_TXEMPTY) == 0);
		}
}

/*
UART Data Available
*/
uint8_t hal_UART_DataAvailable(uint8_t channel)
{
	if(channel<2){
		if ((g_hal_uart_base[channel]->UART_SR & UART_SR_RXRDY) != 0) {
					return 1;
			}
			else {
					return 0;
			}
		}
	else{
		if ((g_hal_usart_base[channel%2]->US_CSR & US_CSR_RXRDY) != 0) {
					return 1;
			}
			else {
					return 0;
			}
		}
};

/*
UART Clear Recieve Interrupt
*/
void hal_UART_ClearRxIF(uint8_t channel){
	hal_UART_DisableRxInterrupt(channel);
}

/*
UART Clear Transmit Interrupt
*/
void hal_UART_ClearTxIF(uint8_t channel){
	hal_UART_DisableTxInterrupt(channel);
}

/*
UART Enable Receive Interrupt
*/
void hal_UART_EnableRxInterrupt(uint8_t channel){
	if(channel<2){
		g_hal_uart_base[channel]->UART_IER= UART_IER_ENDRX;
	}else{
		g_hal_usart_base[channel%2]->US_IER= US_IER_ENDRX;
	}
}

/*
UART Enable Transmit Interrupt
*/
void hal_UART_EnableTxInterrupt(uint8_t channel){

	if(channel<2){
		g_hal_uart_base[channel]->UART_IER= UART_IER_TXEMPTY;
	}else{
		g_hal_usart_base[channel%2]->US_IER= US_IER_TXEMPTY;
	}

}

/*
UART Disable Receive Interrupt
*/
void hal_UART_DisableRxInterrupt(uint8_t channel){
	if(channel<2){
		g_hal_uart_base[channel]->UART_IDR=UART_IDR_ENDRX;
	}else{
		g_hal_usart_base[channel%2]->US_IDR=US_IDR_ENDRX;
	}
}

/*
UART Disable Transmit Interrupt
*/
void hal_UART_DisableTxInterrupt(uint8_t channel){
	if(channel<2){
		g_hal_uart_base[channel]->UART_IDR=UART_IDR_TXEMPTY;
	}else{
		g_hal_usart_base[channel%2]->US_IDR=US_IDR_TXEMPTY;
	}
}


/*
UART Rcv Char
*/
char hal_UART_RxChar(uint8_t channel){
	if(channel<2){
	while ((g_hal_uart_base[channel]->UART_SR & UART_SR_RXRDY) == 0);
	return g_hal_uart_base[channel]->UART_RHR;
	}else{
	while ((g_hal_usart_base[channel%2]->US_CSR & US_CSR_RXRDY) == 0);
	return g_hal_usart_base[channel%2]->US_RHR;
	}
};

/*
UART Tx Char
*/
void hal_UART_TxChar(uint8_t channel, char c){
	 
	if(channel<2){
    while ((g_hal_uart_base[channel]->UART_SR & UART_SR_TXEMPTY) == 0){
    g_hal_uart_base[channel]->UART_THR = c;
		}
	}else{
	while ((g_hal_usart_base[channel%2]->US_CSR & US_CSR_TXEMPTY) == 0){
    g_hal_usart_base[channel%2]->US_THR = c;
		}
	}

};

void hal_UART_Init(uint8_t channel, uint32_t baud) {
	switch(channel) {
		case HAL_UART0:

			break;
		case HAL_UART1:

			break;
		case HAL_UART2:

			break;
		case HAL_UART3:

			break;
		case HAL_UART4:

			break;
		case HAL_UART5:

			break;
		case HAL_UART6:

			break;
		case HAL_UART7:

			break;
		}

}



void hal_UART0_ISR(void){
//	UART_Rx_Handler(UART0);
//	hal_UART_ClearRxIF(UART0);
//	UART_Tx_Handler(UART0);
}
void hal_UART1_ISR(void){
//	UART_Rx_Handler(UART1);
//	hal_UART_ClearRxIF(UART1);
//	UART_Tx_Handler(UART1);
}
void hal_UART2_ISR(void){
//	UART_Rx_Handler(UART2);
//	hal_UART_ClearRxIF(UART2);
//	UART_Tx_Handler(UART2);
}
void hal_UART3_ISR(void){
//	UART_Rx_Handler(UART3);
//	hal_UART_ClearRxIF(UART3);
//	UART_Tx_Handler(UART3);
}
void hal_UART4_ISR(void){
//	UART_Rx_Handler(UART4);
//	hal_UART_ClearRxIF(UART4);
//	UART_Tx_Handler(UART4);
}
void hal_UART5_ISR(void){
//	UART_Rx_Handler(UART5);
//	hal_UART_ClearRxIF(UART5);
//	UART_Tx_Handler(UART5);
}
void hal_UART6_ISR(void){
//	UART_Rx_Handler(UART6);
//	hal_UART_ClearRxIF(UART6);
//	UART_Tx_Handler(UART6);
}
void hal_UART7_ISR(void){
//	UART_Rx_Handler(UART7);
//	hal_UART_ClearRxIF(UART7);
//	UART_Tx_Handler(UART7);
}
