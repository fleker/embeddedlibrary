/*
 * DAq.h
 *
 *  Created on: Mar 27, 2015
 *      Author: Bradley Ebinger
 */

#ifndef INCLUDE_DAQ_H_
#define INCLUDE_DAQ_H_

#define DAQ_BUFFER_SIZE 32

/**
 * Data type definitions that can be used with this module
 */
typedef enum {
	TYPE_UINT8,
	TYPE_UINT16,
	TYPE_UINT32,
	TYPE_FLOAT
} daq_data_type_t;

/**
 * Sampling Frequency Base Factor
 */
typedef enum {
	FREQ_KHZ,
	FREQ_MHZ,
	FREQ_GHZ
} daq_freq_factor_t;

/**
 * Called when the acquisition buffer of size DAQ_BUFFER_SIZE is full and ready to be unloaded.
 * NOTE: Upon this function returning, the data in the buffer will become unusable again. The data must
 * be copied out during this time.
 */
typedef void(*data_ready_cb)(void);
typedef struct {
	uint8_t ID;
	data_ready_cb cb;
	// Define signal characteristics
	daq_freq_factor_t sampFreqFactor;
	float sampFreq;
	daq_data_type_t dataType;
	// Data Buffer
	uint8_t bufferLength;
	uint8_t *dataBuffer;
}daq_id_data_t;

/**
 * Initialize daq module
 */
void DAQ_init(void);

/**
 * Registers a daq_id_data_t structure with the daq aquisition module, allowing for an interrupt-safe buffer to be formed for data aquisition.
 * This function will assign an ID to the device structre. The data_ready_cb callback and sampling frequency/data type should be assigned before
 * DAQ_AcquireData is called.
 */
void DAQ_RegisterDevice(daq_id_data_t *device);

/**
 * Interrupt-safe method used to record data recieved from the device.
 * When the buffer becomes full (set by DAQ_BUFFER_SIZE), the data_ready_cb callback will be called, readying the data to be sent over a communication channel.
 * When the data_ready_cb callback returns, the *dataBuffer pointer is dereferenced to ready for the incoming data.
 */
void DAQ_AcquireData(daq_id_data_t *device, uint8_t *data, uint8_t dataLen);

/**
 * Forces the data_ready_cb callback to be called, making the data acquired in the device buffer available (even if the buffer isn't full).
 */
void DAQ_ForceDataDump(daq_id_data_t *device);

/**
 * Updates the daq module at each frame. This function must be called for the daq module to function.
 */
void DAQ_UpdateTick(void);

#endif /* INCLUDE_DAQ_H_ */
