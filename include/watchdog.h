/* 
 * File:   watchdog.h
 * Author: Michael15
 *
 * Created on July 29, 2015, 11:07 AM
 */

#ifndef _WATCHDOG_H_
#define	_WATCHDOG_H_

#ifdef	__cplusplus
extern "C" {
#endif

#define ResetWatchdog() SetResetWatchdogFlag()
void Watchdog_Init(void);
void SetResetWatchdogFlag(void);

#ifdef	__cplusplus
}
#endif

#endif	/* WATCHDOG_H */

