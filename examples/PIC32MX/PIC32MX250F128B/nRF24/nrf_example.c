#include "system.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define RF_SPI_CH 1
#ifndef SUBSYS_UART
#define SUBSYS_UART 2
#endif

void RF1_CE(uint8_t out);
void RF1_CSN(uint8_t out);
void RF1_PollIRQ(void);

void RF2_CE(uint8_t out);
void RF2_CSN(uint8_t out);
void RF2_PollIRQ(void);

void RF1_Init(void);
void RF2_Init(void);

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF1_AckReceivedHandler(void);
void RF1_MaxRetriesHandler(void);

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckPayloadHandler(uint8_t * data, uint8_t length);
void RF2_AckReceivedHandler(void);
void RF2_MaxRetriesHandler(void);

void RF_Test(void);

#define RF_ADDRESS 0xF0F0F0F0E1LL

nrf24_t RF1;
nrf24_t RF2;

char tx_buf[13] = {"Hello RF2!\r\n"};
char retry_buf[15] = {"Hello Retry!\r\n"};
char ack_buf[10] = {"Hi RF1!\r\n"};

/*
 * main.c
 */
int main(void) {
    DisableInterrupts();

    Timing_Init();
    Task_Init();
    UART_Init(2);

    spi_settings_t spi_settings;
    spi_settings.channel = RF_SPI_CH;
    spi_settings.bit_rate = 100000;
    spi_settings.hal_settings.mode16 = 0;
    spi_settings.hal_settings.mode32 = 0;
    spi_settings.hal_settings.sample_phase = 0;
    spi_settings.mode = 0;
    SPI_Init(&spi_settings);

    EnableInterrupts();

    RF1_Init();
    RF2_Init();

	nRF24_OpenTxPipe(&RF1, RF_ADDRESS);

	nRF24_OpenRxPipe(&RF2, 1, RF_ADDRESS);
	nRF24_StartListening(&RF2);

	Task_Schedule(RF_Test, 0, 500, 0);

	while(1){
		SystemTick();
		RF1_PollIRQ();
		RF2_PollIRQ();
	}
}


/* PIC32MX250F128B RF1:
 * - MISO pin3/A1
 * - MOSI pin24/B13
 * - SCK  pin25/B14
 * - CS   pin7/B3
 * - CE   pin6/B2
 * - IRQ  pin9/A2
 */
void RF1_Init(void){

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB3 = 0; // CS1 output
    TRISBbits.TRISB2 = 0; // CE1 output
    TRISAbits.TRISA2 = 1; // IRQ1 input

    RF1.ce = RF1_CE;
    RF1.csn = RF1_CSN;
    RF1.spi_channel = RF_SPI_CH;
    RF1.ReceivedPayload = RF1_RxPayloadHandler;
    RF1.AckPayloadReceived = RF1_AckPayloadHandler;
    RF1.AckReceived = RF1_AckReceivedHandler;
    RF1.MaxRetriesHit = RF1_MaxRetriesHandler;

    nRF24_Init(&RF1);
    nRF24_SetRetries(&RF1, 0xF, 0x01);
    nRF24_SetChannel(&RF1, 105);
}

void RF1_CE(uint8_t out){
	LATBbits.LATB2 = out ? 1 : 0;
}

void RF1_CSN(uint8_t out){
	LATBbits.LATB3 = out ? 1 : 0;
}

void RF1_PollIRQ(void){
	static uint8_t pin_state = 1;
	uint8_t new_state = PORTAbits.RA2;

	if( (new_state != pin_state) && !new_state) {
		nRF24_ISR(&RF1);
	}
	pin_state = new_state;
}

/* PIC32MX250F128B RF2:
 * - MISO pin3/A1
 * - MOSI pin24/B13
 * - SCK  pin25/B14
 * - CS   pin26/B15
 * - CE   pin2/A0
 * - IRQ  pin10/A3
 */
void RF2_Init(void){

    //Setup IRQ, CE, CSN
    TRISBbits.TRISB15 = 0; // CS2 output
    TRISAbits.TRISA0 = 0; // CE2 output
    TRISAbits.TRISA3 = 1; // IRQ2 input

    RF2.ce = RF2_CE;
    RF2.csn = RF2_CSN;
    RF2.spi_channel = RF_SPI_CH;
    RF2.ReceivedPayload = RF2_RxPayloadHandler;
    RF2.AckPayloadReceived = RF2_AckPayloadHandler;
    RF2.AckReceived = RF2_AckReceivedHandler;
    RF2.MaxRetriesHit = RF2_MaxRetriesHandler;

    nRF24_Init(&RF2);
    nRF24_SetRetries(&RF2, 0xF, 0x01);
    nRF24_SetChannel(&RF2, 105);
}

void RF2_CE(uint8_t out){
	LATAbits.LATA0 = out ? 1 : 0;
}

void RF2_CSN(uint8_t out){
	LATBbits.LATB15 = out ? 1 : 0;
}

void RF2_PollIRQ(void){
	static uint8_t pin_state = 1;
	uint8_t new_state = PORTAbits.RA3;

	if( (new_state != pin_state) && !new_state) {
		nRF24_ISR(&RF2);
	}
	pin_state = new_state;
}


void RF_Test(void){
	nRF24_Write(&RF1, &tx_buf[0], 12);
}

void RF1_AckPayloadHandler(uint8_t * data, uint8_t length){
	UART_Write(SUBSYS_UART, data, length);
}

void RF1_RxPayloadHandler(uint8_t * data, uint8_t length) {
	UART_Write(SUBSYS_UART, data, length);
	nRF24_WriteAck(&RF1, &ack_buf[0], 9, 1);
}

void RF1_AckReceivedHandler(void){
	Task_Queue(RF_Test, 0);
}

void RF1_MaxRetriesHandler(void){
	nRF24_FlushTx(&RF1);
	Task_Queue(RF_Test, 0);
}

void RF2_AckPayloadHandler(uint8_t * data, uint8_t length){
	UART_Write(SUBSYS_UART, data, length);
}

void RF2_RxPayloadHandler(uint8_t * data, uint8_t length) {
	UART_Write(SUBSYS_UART, data, length);
	nRF24_WriteAck(&RF2, &ack_buf[0], 9, 1);
}

void RF2_AckReceivedHandler(void){
	Task_Queue(RF_Test, 0);
}

void RF2_MaxRetriesHandler(void){
	nRF24_FlushTx(&RF2);
	Task_Queue(RF_Test, 0);
}
