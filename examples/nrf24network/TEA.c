#include <stdio.h>
#include <stdint.h>
#include "TEA.h"

#include "big_int.h"



// Change the message from an array of eight char to two uint32_t
void shape_in(char *msg, uint32_t *msg0, uint32_t *msg1) {
  *msg0 = (msg[0]<<24 & 0xFF000000)+(msg[1]<<16 & 0x00FF0000)+(msg[2]<<8 & 0x0000FF00)+(msg[3] & 0x000000FF);
  *msg1 = (msg[4]<<24 & 0xFF000000)+(msg[5]<<16 & 0x00FF0000)+(msg[6]<<8 & 0x0000FF00)+(msg[7] & 0x000000FF);
}

// Change the key from a 128-bit big_int_t to four uint32_t
void shape_key(big_int_t *key, uint32_t *key0, uint32_t *key1, uint32_t *key2, uint32_t *key3) {
  *key0 = (key->bytes[3]<<24)+(key->bytes[2]<<16)+(key->bytes[1]<<8)+key->bytes[0];
  *key1 = (key->bytes[7]<<24)+(key->bytes[6]<<16)+(key->bytes[5]<<8)+key->bytes[4];
  *key2 = (key->bytes[11]<<24)+(key->bytes[10]<<16)+(key->bytes[9]<<8)+key->bytes[8];
  *key3 = (key->bytes[15]<<24)+(key->bytes[14]<<16)+(key->bytes[13]<<8)+key->bytes[12];
}

// Change the message from two uint32_t to an array of eight char
void shape_out(char *msg, uint32_t *msg0, uint32_t *msg1) {
  msg[0] = (*msg0>>24);
  msg[1] = (*msg0>>16) & 0xFF;
  msg[2] = (*msg0>>8) & 0xFF;
  msg[3] = (*msg0) & 0xFF;
  
  msg[4] = (*msg1>>24);
  msg[5] = (*msg1>>16) & 0xFF;
  msg[6] = (*msg1>>8) & 0xFF;
  msg[7] = (*msg1) & 0xFF;
}

// Encrypt the msg with the key
void TEA_encrypt(char *msg, big_int_t *key) {
  uint32_t sum,msg0,msg1,key0,key1,key2,key3;
  sum = 0x00000000;
  
  shape_in(msg,&msg0,&msg1);
  shape_key(key,&key0,&key1,&key2,&key3);
  
  uint8_t i;
  for(i=0;i<32;i++) {
    sum += DELTA;
    msg0 += ((msg1<<4)+key0)^(msg1+sum)^((msg1>>5)+key1);
    msg1 += ((msg0<<4)+key2)^(msg0+sum)^((msg0>>5)+key3);
  }
  
  shape_out(msg,&msg0,&msg1);
}

// Decrypt the msg with the key
void TEA_decrypt(char *msg, big_int_t *key) {
  uint32_t sum,msg0,msg1,key0,key1,key2,key3;
  sum = UNDELTA;

  shape_in(msg,&msg0,&msg1);
  shape_key(key,&key0,&key1,&key2,&key3);

  uint8_t i;
  for(i=0;i<32;i++) {
    msg1 -= ((msg0<<4)+key2)^(msg0+sum)^((msg0>>5)+key3);
    msg0 -= ((msg1<<4)+key0)^(msg1+sum)^((msg1>>5)+key1);
    sum -= DELTA;
  }
  
  shape_out(msg,&msg0,&msg1);
}
